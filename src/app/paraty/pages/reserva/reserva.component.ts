import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ParatyService } from '../../services/paraty.service';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent implements OnInit {

  reservaFormulario: FormGroup;

  reservas: any[] = [];

  public date1: Date;
  public date2: Date;
  public es: any;

  public habitaciones: any[];
  public habitacionSeleccionada: any[];

  constructor(private fb: FormBuilder,
    private _reservaService: ParatyService,
    private toastr: ToastrService) {
    this.reservaFormulario = this.fb.group({
      entrada: ['', Validators.required],
      salida: ['', Validators.required],
      habitacion: ['', Validators.required],

    })
  }

  ngOnInit(): void {
    this.es = {
      firstDayOfWeek: 1,
      dayNames: ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"],
      dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
      dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
      monthNames: ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
      monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"],
      today: 'Hoy',
      clear: 'Borrar'
    }



    this.habitaciones = [
      { name: 'Habitacion 1  / 2 - 0 personas' },
      { name: 'Habitacion 2  / 4 - 0 personas' },
      { name: 'Habitacion 3  / 5 - 0 personas' },
      { name: 'Habitacion 4  / 6 - 0 personas' },
    ]


    this.getReservas()

  }

  agregarHabitacion() {
    const habitacion: any = {
      entrada: this.reservaFormulario.value.entrada,
      salida: this.reservaFormulario.value.salida,
      habitacion: this.reservaFormulario.value.habitacion['name']
    }
    this._reservaService.reservarHabitacion(habitacion).then(() => {
      this.toastr.success('reserva registrado con exito!');
      this.reservaFormulario.reset()
    }).catch(error => {
      console.log(error)
    })
  }


  getReservas() {
    this._reservaService.getReservas().subscribe(data => {
      this.reservas = [];
      data.forEach((element: any) => {
        this.reservas.push(element.payload.doc.data());
      });
      console.log(this.reservas);
    })
  }

}
