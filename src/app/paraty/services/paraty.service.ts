import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ParatyService {

  constructor(private firestore: AngularFirestore) { }


  reservarHabitacion(habitacion: any): Promise<any> {
    return this.firestore.collection('reservas').add(habitacion);
  }

  getReservas(): Observable<any> {
    return this.firestore.collection('reservas').snapshotChanges();
  }
}
