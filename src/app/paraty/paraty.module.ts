import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservaComponent } from './pages/reserva/reserva.component';
import { PrimengModule } from '../primeng/primeng.module';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';






@NgModule({
  declarations: [
    ReservaComponent
  ],
  imports: [
    CommonModule,
    PrimengModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[
    ReservaComponent
  ]

})
export class ParatyModule { }
